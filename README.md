# FranceConnect

This module let you create an OpenID Connect client to use **FranceConnect** as
a Service Provider and log-in or register users within a Drupal site.


## Contents of this file

- Requirements
- Installation
- Configuration
    - Deploying to production
- Information for developers
- Troubleshooting
- Maintainers


## Requirements

This module requires a FranceConnect partner account which can be requested at
[FranceConnect Partner account](https://franceconnect.gouv.fr/partenaires),
Don't forget to mention which personnal data you want to access by referring to
[FranceConnect scope/claims list](https://docs.partenaires.franceconnect.gouv.fr/fs/fs-technique/fs-technique-scope-fc/).

It requires [OpenID Connect](https://www.drupal.org/project/openid_connect)
module version 1.4.


## Installation

Install as you would normally install a contributed Drupal module, using 
Composer is recommended to retrieve **OpenID Connect** module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

First you need to go to
[FranceConnect : Portail Partenaires](https://partenaires.franceconnect.gouv.fr/)
and configure the following:

1. Create a new FS (*Fournisseur de Service*) for your project
1. Copy the **Client ID** (*Identifiant client*) and **Secret Key**
(*Clé secrète*) provided
1. Specify the **Callback URL** (*URLs de callback*) using `https://example.com/openid-connect/franceconnect`
1. Specify the **Logout URL** (*URLs de redirection de déconnexion*) using `https://example.com/user/logout`

Then go back to your site and do the following:
1. Enable FranceConnect and OpenID Connect modules
1. Go to `/admin/config/services/openid-connect` and
    - Enable the FranceConnect OpenID Connect client
    - Fill your **Client ID** and **Secret Key**
    - Fill the various endpoints using the *integration URLs* for testing, the
full list is [here](https://partenaires.franceconnect.gouv.fr/fcp/fournisseur-service)
    - Map the user claims to your user fields.

1. Go to `/admin/structure/block` and place the **FranceConnect login** block
in the "Content" region and restrict it to the login and register page.
1. Go to `/user/login` and you should see the FranceConnect login button, click
it and choose a provider for which you have a FranceConnect account or you can
use the test provider (*Démontrastion eIDAS faible*) and one of the
[test accounts](https://github.com/france-connect/identity-provider-example/blob/master/database.csv).


### Deploying to production

Once you managed to test your site in the "Integration" environment, you need to
ask for an official review in order to get the production keys.
Go to https://www.demarches-simplifiees.fr/commencer/demande-qualification-fs
and follow the procedure.

When you have your production keys, update them in
`/admin/config/services/openid-connect` and don't forget to also update the
endpoints.


## Information for developers

The module is currently using the EIDAS standard level `eidas1` by declaring an
extra `acr_values` claim in the call see `src/Plugin/OpenIDConnectClient/FranceConnect.php`.  
If you need higher standard this line should be updated.

By default **FranceConnect** will return basic identity claims (`given_name`,
`family_name`, `birthdate` (format `YYYY-MM-DD`), `gender`, `email` and `sub`).  
You can implement additional claims using `hook_openid_connect_claims_alter()`.

```php
/**
 * Implements hook_openid_connect_claims_alter().
 * 
 * Adds claim for FranceConnect mapping.
 * See https://docs.partenaires.franceconnect.gouv.fr/fs/fs-technique/fs-technique-scope-fc/
 *
 * @param $claims
 */
function HOOK_openid_connect_claims_alter(&$claims) {
  $claims['birthcountry'] = [
    'scope' => 'identite_pivot',
    'title' => t('Birth country'),
    'type' => 'string',
    'description' => t('INSEE code for the birth country, see the <a href="@url" target="_blank">list</a>.', ['@url' => 'https://www.insee.fr/fr/information/2560452']),
    ];
```

When receiving the datas from FranceConnect, you can alter them using
`hook_openid_connect_post_authorize()`.

```php
/**
 * Implements hook_openid_connect_post_authorize().
 * Save the birth country only for France.
 *
 * @param \Drupal\user\UserInterface $account
 *   User account object of the authorized user.
 * @param array $context
 *   An associative array with context information:
 *   - tokens:         An array of tokens.
 *   - user_data:      An array of user and session data.
 *   - userinfo:       An array of user information.
 *   - plugin_id:      The plugin identifier.
 *   - sub:            The remote user identifier.
*
* @ingroup openid_connect_api
*/
function HOOK_openid_connect_post_authorize(UserInterface $account, array $context) {
  if (isset($context['userinfo']['birthcountry']) && $context['userinfo']['birthcountry'] == '99100') {
    $account->field_brith_country->value = 'FR';
  }
}
```


## Troubleshooting

The errors returned by FranceConnect are listed in the 
[official documentation](https://partenaires.franceconnect.gouv.fr/fcp/fournisseur-service).

- `E000009`: the `redirect_uri` difers from the one registered in your partner
account.
- `E000019`: appears when the *Client ID* and *Secret key* aren't correct or
don't match the endpoints (ex: mixing *integration* values with *production*
endpoint).

## Maintainers

- Nicolas Tostin - [tostinni](https://www.drupal.org/u/tostinni)